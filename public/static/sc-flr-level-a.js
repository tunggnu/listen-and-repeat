/*
 * The code in this file is meant to override the features in the base.js file
 */

playbackRateControl.value = 1;
playbackRateValue.textContent = playbackRateControl.value;
