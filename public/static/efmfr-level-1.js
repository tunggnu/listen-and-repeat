/*
 * The code in this file is meant to override the features in the base.js file
 */

playbackRateControl.value = 0.9;
playbackRateValue.textContent = playbackRateControl.value;
