document.querySelector('#home').insertAdjacentHTML(
    'afterend',
    `
<div id="the-options">
<div class="the-option">
    <label for="skip-first-control">Bỏ câu đầu</label>
    <input id="skip-first-control" class="skip-first-control" type="checkbox" />
    <span class="skip-first-value">Không</span>
</div>
<div class="the-option">
    <label for="skip-last-control">Bỏ câu cuối</label>
    <input id="skip-last-control" class="skip-last-control" type="checkbox" />
    <span class="skip-last-value">Không</span>
</div>
<div class="the-option">
    <label for="playback-rate-control">Tốc độ đọc</label>
    <input id="playback-rate-control" class="playback-rate-control" type="range" min="0.75" max="1.25"
        step="0.05" value="0.85" />
    <span class="playback-rate-value">0.85</span>x
</div>
<div class="the-option">
    <label for="read-along-control">Đợi đọc theo</label>
    <input id="read-along-control" class="read-along-control" type="checkbox" checked />
    <span class="read-along-value">Có</span>
</div>
<div class="the-option">
    <label for="rest-duration-control">Nghỉ hết câu</label>
    <input id="rest-duration-control" class="rest-duration-control" type="range" min="0.5" max="1.5"
        step="0.25" value="0.75" />
    <span class="rest-duration-value">0.75</span> giây
</div>
<div class="the-option">
    <label for="repeat-sentence-control">Lặp mỗi câu</label>
    <input id="repeat-sentence-control" class="repeat-sentence-control" type="range" min="1" max="5" step="1"
        value="3" />
    <span class="repeat-sentence-value">3</span> lần
</div>
<div class="the-option">
    <label for="repeat-all-control">Lặp toàn bộ</label>
    <input id="repeat-all-control" class="repeat-all-control" type="checkbox" />
    <span class="repeat-all-value">Không</span>
</div>
</div>

<div id="the-control">
<button class="move back disabled">Trước</button>
<button class="play disabled">Đọc</button>
<button class="pause disabled">Dừng</button>
<button class="move next disabled">Sau</button>
</div>

<h1 name="the-text" id="the-text"></h1>
<h2 id="the-page-number"></h2>`
);

// DOM nodes
const theText = document.querySelector("#the-text");
const thePageNumber = document.querySelector('#the-page-number');

const skipFirstControl = document.querySelector(".skip-first-control");
const skipFirstValue = document.querySelector('.skip-first-value');

const skipLastControl = document.querySelector(".skip-last-control");
const skipLastValue = document.querySelector('.skip-last-value');

const playButton = document.querySelector(".play");
playButton.setAttribute("disabled", "disabled");

const stopButton = document.querySelector(".pause");
stopButton.setAttribute("disabled", "disabled");

const backButton = document.querySelector(".back");
backButton.setAttribute("disabled", "disabled");

const nextButton = document.querySelector(".next");
nextButton.setAttribute("disabled", "disabled");

const playbackRateControl = document.querySelector(".playback-rate-control");
const playbackRateValue = document.querySelector(".playback-rate-value");
playbackRateControl.setAttribute("disabled", "disabled");

const restDurationControl = document.querySelector(".rest-duration-control");
const restDurationValue = document.querySelector(".rest-duration-value");

const repeatSentenceControl = document.querySelector('.repeat-sentence-control');
const repeatSentenceValue = document.querySelector('.repeat-sentence-value');

const repeatAllControl = document.querySelector('.repeat-all-control');
const repeatAllValue = document.querySelector('.repeat-all-value');

const readAlongControl = document.querySelector('.read-along-control');
const readAlongValue = document.querySelector('.read-along-value');

playButton.onclick = () => {
    playbackPaused = false;
    thisPage.audio.play();
    playButton.setAttribute("disabled", "disabled");
    playButton.classList.add("disabled");
    stopButton.classList.remove("disabled");
    stopButton.removeAttribute("disabled");
};

stopButton.onclick = () => {
    playbackPaused = true;
    thisPage.audio.pause();

    playButton.removeAttribute("disabled");
    playButton.classList.remove("disabled");
    stopButton.setAttribute("disabled", "disabled");
    stopButton.classList.add("disabled");
};

backButton.onclick = () => {
    playbackPaused = true;
    thisPage.audio.pause();
    thisPage.audio.currentTime = 0;

    stopButton.setAttribute("disabled", "disabled");
    stopButton.classList.add("disabled");

    if (thisPage.previousPage === null) {
        thisPage = backCover;
    } else {
        thisPage = thisPage.previousPage;
    }

    if (skipFirst && (thisPage === frontCover)) {
        thisPage = backCover;
    }

    if (skipLast && (thisPage === backCover)) {
        thisPage = thisPage.previousPage;
    }

    updateTextAndPageNumber();

    playButton.removeAttribute("disabled");
    playButton.classList.remove("disabled");
};

nextButton.onclick = () => {
    playbackPaused = true;
    thisPage.audio.pause();
    thisPage.audio.currentTime = 0;

    stopButton.setAttribute("disabled", "disabled");
    stopButton.classList.add("disabled");

    if (thisPage.nextPage === null) {
        thisPage = frontCover;
    } else {
        thisPage = thisPage.nextPage;
    }

    if (skipLast && (thisPage === backCover)) {
        thisPage = frontCover;
    }

    if (skipFirst && (thisPage === frontCover)) {
        thisPage = pages[1];
    }

    updateTextAndPageNumber();

    playButton.removeAttribute("disabled");
    playButton.classList.remove("disabled");
};

playbackRateControl.oninput = () => {
    pages.forEach(page => {
        page.audio.playbackRate = playbackRateControl.value;
    });

    playbackRateValue.textContent = playbackRateControl.value;
};

restDurationControl.oninput = () => {
    restDurationValue.textContent = restDurationControl.value;
};

repeatSentenceControl.oninput = () => {
    repeatSentence = repeatSentenceControl.value;
    repeatSentenceValue.textContent = repeatSentence;
};

repeatAllControl.oninput = () => {
    repeatAll = repeatAllControl.checked;
    repeatAllValue.textContent = repeatAll ? TEXTS.yes : TEXTS.no;
};

readAlongControl.oninput = () => {
    readAlong = readAlongControl.checked;
    readAlongValue.textContent = readAlong ? TEXTS.yes : TEXTS.no;
};

skipFirstControl.oninput = () => {
    skipFirst = skipFirstControl.checked;
    skipFirstValue.textContent = skipFirst ? TEXTS.yes : TEXTS.no;
    firstPage = skipFirst ? frontCover.nextPage : frontCover;

    if (skipFirst && (thisPage === frontCover)) {
        thisPage.audio.pause();
        thisPage.audio.currentTime = 0;
        thisPage = firstPage;

        updateTextAndPageNumber();

        playButton.removeAttribute("disabled");
        playButton.classList.remove("disabled");
        stopButton.setAttribute("disabled", "disabled");
        stopButton.classList.add("disabled");
    }
};

skipLastControl.oninput = () => {
    skipLast = skipLastControl.checked;
    skipLastValue.textContent = skipLast ? TEXTS.yes : TEXTS.no;
    lastPage = skipLast ? backCover.previousPage : backCover;

    if (skipLast && (thisPage === backCover)) {
        thisPage.audio.pause();
        thisPage.audio.currentTime = 0;
        thisPage = firstPage;

        updateTextAndPageNumber();

        playButton.removeAttribute("disabled");
        playButton.classList.remove("disabled");
        stopButton.setAttribute("disabled", "disabled");
        stopButton.classList.add("disabled");
    }
};

// Logic
let skipFirst = false,
    skipLast = false,
    repeatAll = false,
    playbackPaused = false,
    readAlong = readAlongControl.checked,
    allAudioLoaded = 0,
    previousPage = null,
    thisPage = null,
    repeatSentence = repeatSentenceControl.value,
    showNewText = null,
    playNewAudio = null,
    frontCover = null,
    backCover = null,
    firstPage = null,
    lastPage = null,
    pages = [];

const TEXTS = {
    yes: 'Có',
    no: 'Không',
}

const fromShowingTextToReadingTextTime = 500;

const pageCount = pageTexts.length;

function updateTextAndPageNumber() {
    theText.innerHTML = thisPage.text;
    thePageNumber.textContent = '(' + thisPage.pageNumber + '/' + pageCount + ')';
}

window.addEventListener("load", () => {
    pages = pageTexts.map(
        (value, index) => {
            const pageNumber = index + 1;
            const audioUrl = 'audio/' + pageNumber.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }) + '.mp3';
            const audio = new Audio(audioUrl);
            audio.addEventListener("canplaythrough", () => {
                page.length = audio.duration;

                allAudioLoaded++;

                if (allAudioLoaded === pageTexts.length) {
                    playButton.removeAttribute("disabled");
                    playButton.classList.remove("disabled");
                    backButton.removeAttribute("disabled");
                    backButton.classList.remove("disabled");
                    nextButton.removeAttribute("disabled");
                    nextButton.classList.remove("disabled");
                    playbackRateControl.removeAttribute("disabled");
                }
            });

            const page = {
                text: value.replaceAll('\n', '<br>'),
                audio: audio,
                nextPage: null,
                previousPage: previousPage,
                looped: 0,
                pageNumber: pageNumber,
            };

            if (previousPage !== null) {
                previousPage.nextPage = page;
            }

            previousPage = page;

            audio.addEventListener("ended", () => {
                let restTime = Math.floor(restDurationControl.value * 1000);

                // Take more time to rest if read along.
                // Do not read along the front and back covers.
                if (readAlong &&
                    (thisPage !== frontCover) &&
                    (!READ_BACK_COVER || (thisPage !== backCover))
                ) {
                    restTime += Math.floor(thisPage.length * 1000 / playbackRateControl.value);
                }

                // Move to the next page if loop enough.
                // Do not loop the front and back covers.
                thisPage.looped++;
                if ((thisPage.looped >= repeatSentence) ||
                    (thisPage === frontCover) ||
                    (READ_BACK_COVER && (thisPage === backCover))
                ) {
                    // Reset the loop count
                    thisPage.looped = 0;

                    if (thisPage !== lastPage) {
                        // Only read the front cover once
                        if (thisPage === frontCover) {
                            firstPage = frontCover.nextPage;
                            skipFirstControl.checked = true;
                            skipFirstValue.textContent = TEXTS.yes;
                            skipFirst = true;
                        }

                        // This page is not the last, move to the next page
                        thisPage = thisPage.nextPage;
                    } else {
                        // Last page has been read
                        if (repeatAll) {
                            // Only read the back cover once
                            if (READ_BACK_COVER && (thisPage === backCover)) {
                                lastPage = backCover.previousPage;
                                skipLastControl.checked = true;
                                skipLastValue.textContent = TEXTS.yes;
                                skipLast = true;
                            }

                            // Move to the first page to read again
                            thisPage = firstPage;
                        } else {
                            // Stop reading
                            thisPage = null;
                        }
                    }
                }

                // Prepare to read the text if any
                if (thisPage !== null) {
                    // Show the new text
                    showNewText = setTimeout(
                        () => updateTextAndPageNumber(),
                        restTime - fromShowingTextToReadingTextTime
                    );
                    playNewAudio = setTimeout(
                        () => playbackPaused || thisPage.audio.play(),
                        restTime
                    );

                    return;
                }

                // Stop reading
                thisPage = firstPage;
                playbackPaused = false;
                updateTextAndPageNumber();
                playButton.removeAttribute("disabled");
                playButton.classList.remove("disabled");
                stopButton.setAttribute("disabled", "disabled");
                stopButton.classList.add("disabled");
            });

            return page;
        }
    );

    if (!READ_BACK_COVER) {
        // The back cover is not included, disable skipping back cover
        skipLastControl.checked = false;
        skipLastControl.disabled = true;
        skipLastValue.textContent = TEXTS.no;
        skipLast = false;
    } else {
        // Default to skip the back cover
        skipLastControl.checked = true;
        skipLastValue.textContent = TEXTS.yes;
        skipLast = true;
    }

    thisPage = firstPage = frontCover = pages[0];
    backCover = pages[pageCount - 1];
    lastPage = skipLast ? backCover.previousPage : backCover;

    updateTextAndPageNumber();

    // Set default playback speed
    pages.forEach(page => {
        page.audio.playbackRate = playbackRateControl.value;
    });
});
